from tkinter import *
from tkinter import messagebox
import numpy_financial as npf


def hallar_TIR_pesim(inver, ganancias):
    original = [inver]
    cont = -1
    for i in range(len(ganancias)):
        original.append(ganancias[i])
        cont = cont+1;

    res = round(npf.irr(original), cont)
    porcent = res*100
    print("El valor de TIR pésimista es: "+str(porcent)+"%")
    return porcent

   
def datos_ganacia():
    datos = []
    for x in range(5):
        ingresa = "Ingrese por favor las ganacias del año: "+ str(x+1) + ": "
        dato = int(input(ingresa))
        datos.append(dato)
    return datos


def hallar_TIR_optim():
    inver = [-60000,-25000,45000, 12000, 12000, 15000, 18000, 19000]
    res = round(npf.irr(inver), 5)
    porcent = res*100
    print("El TIR optimo es de: "+ str(porcent))
    return porcent

def dividir_en_interv(cantInter, inver):
    interv_ini = inver
    interv_fini = hallar_TIR_optim()
    tam_total = interv_fini - interv_ini
    tam_intervalos = tam_total /cantInter
    print("La cantidad de intervalos a subdividir :"+str(cantInter)+" cada intervalo es de: "+str(tam_intervalos))
    return tam_intervalos


ganancia = datos_ganacia()
invers = hallar_TIR_pesim(-100000, ganancia)
dividir_en_interv(20, invers)

